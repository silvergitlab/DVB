# DVB-S/S2
---
This document is covered by the GNU General Public License (GNU GPL). If you haven’t read the GPL before, please do so. It explains all the things that you are allowed to do with this documentation.

October 12 2015

:copyright: Alireza Pazhouhesh

[lidora.blog.ir](http://lidora.blog.ir)

---
### Table of contents
* [Prof 7500 usb installation](#Prof 7500 usb installation)
* [LinuxTV dvb-apps installation](#LinuxTV dvb-apps installation)
    * [List of utilities within dvb-apps](#List of utilities within dvb-apps)
* [Get missing transporders list](#Get missing transporders list)
* [scan](#scan)
* [dvbscan](#dvbscan)
* [w_scan](#w_scan)
* [w_scan sattelites list](#w_scan sattelites list)

---
<a name="Prof 7500 usb installation"/>
### Prof 7500 usb installation
* The firmware can be downloaded from [this (Russian) forum](http://www.forum.free-x.de/wbb/index.php?page=Thread&threadID=798).  
* Rename `dvb-usb-p7500.fw.txt` to `dvb-usb-p7500.fw`
* Put it into `/lib/firmware`. Restart and finish.

---
<a name="LinuxTV dvb-apps installation"/>
### LinuxTV dvb-apps installation
* hg clone http://linuxtv.org/hg/dvb-apps   # download the tarball link below and extract if you don't have mercurial.
* cd dvb-apps
* make
* sudo make install


<a name="List of utilities within dvb-apps"/>
### List of utilities within `dvb-apps`
* Main User Applications:
    * `scan`: the original frequency scanning tool used to generate channel lists
    * `dvbscan`: another frequency scanning tool used to generate channel lists .... some distro package managers have rebranded this as "scandvb" ... also note that "atscscan", if included, is simply a copy of dvbscan. The DVB or ATSC protocol is determined by a tuning file feed to these programs. Tuning files are kept in the scan/atsc, scan/dvb-[csth] directories. This program discovers carriers on candidate frequencies and also determines sub-channels. sub-channels may be the main program at a lower data rate or completely new content. With the output produced, a short name can be used to identify the frequency and the streams needed to get a channel.
    * `czap`, `szap`, `tzap`: tuning utilities for DVB-C, DVB-S, DVB-T. These change the channel/sub-channel as identified by the file output by scan above.
    * `azap`: tuning utility for ATSC.
    * `util/gnutv` - Tune, watch and stream your TV. I.e. a DVB UI.

* General Utilities:
    * `util/dvbdate` - Read date time information from the currently tuned multiplex.
    * `util/dvbnet` - Control digital data network interfaces. DVB network interface manager (IP over DVB).
    * `util/dvbtraffic` - Monitor traffic on a digital device. PID analysis of currently tuned multiplex.
    * `util/femon` - Frontend (fe) monitor. Monitor the tuning status on a digital TV device.
    * `util/zap` - *Just* tunes a digital device - really intended for developers. Note that this is a seperate app then those *zap utilities listed above.
    * `util/atsc_epg` - Recent versions of dvb-apps include a program to print out the next three hours worth of programs on a given frequency (for ATSC only).

* Hardware Specific Utilities:
    * `util/av7110_loadkeys` - A utiltity to load IR remote keymaps into an av7110 based card using the /proc/av7110_ir interface
    * `util/dib3000-watch` - Monitor DIB3000 demodulators
    * `util/dst-utils/dst-test` - Utilities for DST based cards.
    * `util/ttusb_dec_reset` - Reset a TechnoTrends TTUSB DEC device.

---
<a name="Get missing transporders list"/>
### Get missing transporders list
Use this PKGBUILD
```
# $Id$
# Maintainer: peace4all <markspost at rocketmail dot com>

pkgname=dtv-scan-tables-git
_gitname=dtv-scan-tables
pkgver=1850cf8
pkgrel=1
pkgdesc='Digital TV scan tables'
arch=('any')
url='https://github.com/oliv3r/dtv-scan-tables'
license=('GPL2')

makedepends=('git')
source=($_gitname::git+https://github.com/oliv3r/dtv-scan-tables.git)
sha256sums=('SKIP')

pkgver(){
  cd "${srcdir}/dtv-scan-tables"
  git describe --always | sed 's|-|.|g'
}

package() {
  cd "${srcdir}/dtv-scan-tables"
  install -d ${pkgdir}/usr/share/dvb
  mv ${srcdir}/dtv-scan-tables/{atsc,dvb-c,dvb-s,dvb-t} ${pkgdir}/usr/share/dvb
  find "${pkgdir}" -type d | xargs chmod 755
  find "${pkgdir}" -type f | xargs chmod 644
}
```

---
<a name="scan"/>
### scan

* Documentation

    -?      |   Description
    ---     |   ---
    -c      |   scan on currently tuned transponder only
    -v      |   verbose (repeat for more)
    -q      |   quiet (repeat for less)
    -a N    |   use DVB /dev/dvb/adapterN/
    -f N    |   use DVB /dev/dvb/adapter?/frontendN
    -d N    |   use DVB /dev/dvb/adapter?/demuxN
    -s N    |   use DiSEqC switch position N (DVB-S only)
    -i N    |   spectral inversion setting (0: off, 1: on, 2: auto [default])
    -n      |   evaluate NIT-other for full network scan (slow!)
    -5      |   multiply all filter timeouts by factor 5 for non-DVB-compliant section repitition rates
    -o      |   fmt  output format: 'zap' (default), 'vdr' or 'pids' (default with -c)
    -x N    |   Conditional Access, (default -1). <br />N=0 gets only FTA channels. <br />N=-1 gets all channels. <br />N=xxx sets ca field in vdr output to :xxx:
    -t N    |   Service select, Combined bitfield parameter. 1 = TV, 2 = Radio, 4 = Other, (default 7)
    -p      |   for vdr output format: dump provider name
    -e N    |   VDR version, default 3 for VDR-1.3.x and newer. value 2 sets NIT and TID to zero
    -l lnb-type |   (DVB-S Only) (use -l help to print types) or
    -l low[,high[,switch]]  |   in Mhz
    -u      |   UK DVB-T Freeview channel numbering for VDR
    -P      |   do not use ATSC PSIP tables for scanning (but only PAT and PMT) (applies for ATSC only)
    -A N    |   check for ATSC 1=Terrestrial [default], 2=Cable or 3=both
    -U      |   Uniquely name unknown services

* Usage
    * scan comes with a list of satellites (for dvb-s) 
        * ```~$ scan /usr/share/dvb/dvb-s/ [pressing ''Tab'', reveals the list]```
    * and terrestrial transmitters (for dvb-t) 
        * ```~$ scan /usr/share/dvb/dvb-t/uk- [pressing ''Tab'', reveals the list]```
    * scan for mplayer
        * ```scan -s 3 -p -o vdr -e 3 /usr/share/dvb/dvb-s/EutelsatW2-16E | tee channels.conf```
        
---
<a name="dvbscan"/>
### dvbscan

* Documentation

    |-?                         |  Description  |
    |------------               |---------------|
    |-h                         |  help |
    | dadsad                    | asdasd |
    | -adapter <id>             | adapter to use (default 0)` |
    | -frontend <id>            | frontend to use (default 0) |
    | -demux <id>               | demux to use (default 0) |
    | -secfile <filename>       | Optional sec.conf file. |
    | -secid <secid>            | ID of the SEC configuration to use, one of: <br />* UNIVERSAL (default) - Europe, 10800 to 11800 MHz and 11600 to 12700 Mhz, Dual LO, loband 9750, hiband 10600 MHz. <br />* DBS - Expressvu, North America, 12200 to 12700 MHz, Single LO, 11250 MHz. <br />* STANDARD - 10945 to 11450 Mhz, Single LO, 10000 Mhz. <br />* ENHANCED - Astra, 10700 to 11700 MHz, Single LO, 9750 MHz. <br />* C-BAND - Big Dish, 3700 to 4200 MHz, Single LO, 5150 Mhz. <br />* C-MULTI - Big Dish - Multipoint LNBf, 3700 to 4200 MHz, Dual LO, H:5150MHz, V:5750MHz. <br />* One of the sec definitions from the secfile if supplied |
    | -satpos <position>        | Specify DISEQC switch position for DVB-S. |
    | -inversion <on|off|auto>  | Specify inversion (default: auto). |
    | -uk-ordering              | Use UK DVB-T channel ordering if present. |
    | -timeout <secs>           | Specify filter timeout to use (standard specced values will be used by default) |
    | -filter <filter>          | Specify service filter, a comma seperated list of the following tokens: (If no filter is supplied, all services will be output).<br /> * tv - Output TV channels. radio - Output radio channels.<br />* other - Output other channels.<br />* encrypted - Output encrypted channels |
    | -out raw <filename>-      | Output in raw format to <filename> or stdout channels | <filename>- Output in channels.conf format to <filename> or stdout. |
    | -out vdr12 <filename>-    | Output in vdr 1.2.x format to <filename> or stdout. | 
    | -out vdr13 <filename>-    | Output in vdr 1.3.x format to <filename> or stdout.
<initial scan file> |
* Usage
    * `$ dvbscan /usr/local/share/dvb/scan/dvb-t/au-Adelaide`
    * `$ dvbscan /usr/share/doc/dvb-utils/examples/scan/dvb-t/uk-Oxford`

---
<a name="w_scan"/>
### w_scan
* Documentation

    -?          |  Description 
    ------------|---------------
    -f | Frontend Type<br />a = ATSC, siehe auch Option A<br />c = DVB-C<br />s = DVB-S/S2<br />t = DVB-T (Voreinstellung)
    -c | Land<br />Angabe des Landes for ATSC und DVB-C/T<br />HINWEIS: Ab w_scan-200905xx, -c? for Liste (Pflichtangabe.)
    -s | Satellit<br />Angabe des Satelliten for DVB-S/S2<br />HINWEIS: Ab w_scan-200905xx, -c? for Liste (Pflichtangabe.)
    -A | N Angabe ATSC Typ (Kabel/Terrestrisch)<br />1 = Terrestrisch (VSB) (default)<br />2 = Kabel<br />3 = beides, Terrestrisch und Kabel
    -o | N optional: explizite Angabe der VDR Version<br />2  = VDR-2.0 (Voreinstellung)<br />21 = VDR-2.1
    -X | czap/tzap/xine Ausgabe Format<br />Alternatives Ausgabeformat der channels.conf, passend for tzap, czap, xine usw.
    -M | mplayer Ausgabe Format<br />Alternatives Ausgabeformat der channels.conf, passend for mplayer, wie -X aber ohne Providername und multiple Audio-Pids
    -x | initial tuning data for (dvb-)scan erzeugen
    -k | channels.dvb for kaffeine-0.8.5 erzeugen
    -I | Import von (dvb)scan initial-tuning-data (ab 200905xx)
    -H | Hilfe for erweiterte Optionen

* Usage
    * DVB-T/T2 VDR (Land = Deutschland):
        * `w_scan -ft -c DE>> /etc/vdr/channels.conf`
    * DVB-T/T2 VDR (Land = United Kingdom):
        * `w_scan -ft -c GB>> /etc/vdr/channels.conf`
    * DVB-C VDR:
        * `w_scan -fc >> /etc/vdr/channels.conf`
    * DVB-T/T2 und DVB-C mit VDR:
        * `w_scan -ft -c DE >> /etc/vdr/channels.conf && w_scan -fc -c DE >> /etc/vdr/channels.conf`
    * DVB-S/S2, VDR, Astra S19.2E:
        * `w_scan -fs -sS19E2 -o7 >> /etc/vdr/channels.conf`
    * DVB-S/S2, VDR, Hotbird S13.0E:
        * `w_scan -fs -sS13E0 >> /etc/vdr/channels.conf`
    * DVB-S/S2, Ku-Badr, VLC Format:
        * `w_scan -fs -s S26E0 -L > ku-badr-4-5-6.xspf`
    * Optionen anzeigen lassen:
        * `w_scan -h`
    * Erweiterte Optionen anzeigen lassen:
        * `w_scan -H`
    * DVB-T initial-tuning-data erzeugen:
        * `w_scan -ft -c DE -x > initial-tuning-data.txt`
    * DVB-C initial-tuning-data erzeugen:
        * `w_scan -fc -c DE -x > initial-tuning-data.txt`
    * DVB-S initial-tuning-data erzeugen:
        * `w_scan -fs -s S19E2 -x > initial-tuning-data.txt`
    * DVB-T kaffeine channels.dvb erzeugen:
        * `w_scan -k > channels.dvb`
    * DVB-C kaffeine channels.dvb erzeugen:
        * `w_scan -fc -k > channels.dvb`
    * ATSC terrestrisch (VSB) scannen:
        * `w_scan -fa > channels.conf`
    * ATSC Kabel TV (US QAM Annex-B):
        * `w_scan -A2 > channels.conf`
    * ATSC terrestrisch (VSB) und Kabel (US QAM-B):
        * `w_scan -A3 > channels.conf`
    * UTF-8 Ausgabe:
        * `w_scan -C utf-8 (weitere Optionen)`

---
<a name="w_scan sattelites list"/>
### w_scan sattelites list

Code    |   Name
--------|--------------------------
S180E0	|	180.0 east Intelsat 18
S172E0	|	172.0 east GE 23
S169E0	|	169.0 east Intelsat
S166E0	|	166.0 east Intelsat 8
S162E0	|	162.0 east Superbird B2
S160E0	|	160.0 east Optus D1
S156E0	|	156.0 east Optus C1/D3
S154E0	|	154.0 east JCSAT 2A
S152E0	|	152.0 east Optus D2
S144E0	|	144.0 east Superbird C2
S140E0	|	140.0 east Express AM3
S138E0	|	134.0 east Telstar 18
S134E0	|	134.0 east Apstar 6
S132E0	|	132.0 east Vinasat 1 / JCSAT 5A
S128E0	|	128.0 east JCSAT 3A
S125E0	|	125.0 east ChinaSat 6A
S124E0	|	124.0 east JCSAT 4A
S122E2	|	122.2 east AsiaSat 4
S118E0	|	118.0 east Telkom 2
S116E0	|	116.0 east Koreasat 6 / ABS 7
S115E5	|	115.5 east ChinaSat 6B
S113E0	|	113.0 east Koreasat 5 / Palapa D
S110E5	|	110.5 east ChinaSat 10
S110E0	|	110.0 east NSat 110 / BSat 2C/3A / JCSat 110R
S108E2	|	108.2 east Telkom 1 / NSS 11 / SES 7
S105E5	|	105.5 east AsiaSat 3S
S103E0	|	103.0 east Express A2
S100E5	|	100.5 east AsiaSat 5
S96E5	|	96.5 east Express AM33
S95E0	|   95.0 east NSS 6
S93E5	|	93.5 east Insat 3A/4B
S91E5	|	91.5 east Measat 3 / 3A
S90E0	|	90.0 east Yamal 201
S88E0	|	88.0 east ST 2
S87E5	|	87.5 east ChinaSat 5A
S86E5	|	86.5 east KazSat 2
S85E0	|	85.0 east Horisons 2 / Intelsat 15
S83E0	|	83.0 east Insat 2E/4A
S78E5	|	78.5 east Thaicom 2/5
S76E5	|	76.5 east Telstar 10 (Apstar 2R)
S75E0	|	75.0 east ABS 1
S70E5	|	70.5 east Eutelsat W5
S68E5	|	68.5 east Intelsat 7/10
S66E0	|	66.0 east Intelsat 17
S64E2	|	64.2 east Intelsat 906
S62E0	|	62.0 east Intelsat 902
S60E0	|	60.0 east Intelsat 904
S57E0	|	57.0 east NSS 12
S56E0	|	56.0 east Bonum 1
S53E0	|	53.0 east Express AM22
S52E5	|	52.5 east Yahsat 1A
S49E0	|	49.0 east Yamal 202
S45E0	|	45.0 east Intelsat 12
S42E0	|	42.0 east Turksat 2A/3A
S40E0	|	40.0 east Express AM1
S39E0	|	39.0 east Hellas Sat 2
S38E0	|	38.0 east Paksat 1
S36E0	|	36.0 east Eutelsat W4/W7
S33E0	|	33.0 east Eurobird 3
S31E5	|	31.5 east Astra 5A/1D
S30E5	|	30.5 east Arabsat 5A
S28E2	|	28.2 east Astra 2A/B/C/D + EuroBird 1 28.5
S26E0	|	26.X east Badr C/3/4/6
S25E5	|	25.5 east Eurobird 2
S23E5	|	23.5 east Astra 1E/3A
S21E6	|	21.6 east Eutelsat W6
S20E0	|	20.0 east Arabsat 5C
S19E2	|	19.2 east Astra 1F/1G/1H/1KR/1L
S16E0	|	16.0 east Eutelsat W2
S13E0	|	13.0 east Hotbird 6/7A/8
S10E0	|	10.0 east Eutelsat W1
S9E0	|	9.0 east Eurobird 9
S7E0	|	7.0 east Eutelsat W3A
S4E8	|	4.8 east Sirius
S3E0	|	3.0 east Eutelsat 3A/3C, Rascom 1R
S0W8	|	0.8 west Thor 3/5 & Intelsat 10-02
S4W0	|	4.0 west Amos 1/2/3
S5W0	|	5.0 west Atlantic Bird 3/ Eutelsat 5A
S7W0	|	7.0 west Nilesat 101/102 & Atlantic Bird 4
S8W0	|	8.0 west Atlantic Bird 2
S11W0	|	11.0 west Express AM44
S12W5	|	12.5 west Atlantic Bird 1
S14W0	|	14.0 west Express A4
S15W0	|	15.0 west Telstar 12
S18W0	|	18.0 west Intelsat 901
S20W0	|	20.0 west NSS 5
S22W0	|	22.0 west NSS 7
S24W5	|	24.5 west Intelsat 905
S27W5	|	27.X west Intelsat 907
S30W0	|	30.0 west Hispasat 1C/1D
S31W5	|	31.5 west Intelsat 25
S34W5	|	34.5 west Intelsat 903
S37W5	|	37.5 west NSS 10 / Telstar 11N
S40W5	|	40.5 west NSS 806
S43W0	|	43.0 west Intelsat 11
S45W0	|	45.0 west Intelsat 14
S50W0	|	50.0 west Intelsat 1R
S53W0	|	53.0 west Intelsat 707
S55W5	|	55.5 west Intelsat 805
S58W0	|	58.0 west Intelsat 9/16
S63W0	|	63.0 west Telstar 14R
S65W0	|	65.0 west Star One C1
S70W0	|	70.0 west Star One C2
S72W0	|	72.0 west AMC 6
S78W0	|	78.0 west SIMON BOLIVAR
S83W0	|	83.0 west AMC 9
S84W0	|	84.0 west Brasilsat B4
S85W0	|	85.0 west AMC 16
S87W0	|	87.0 west SES 2
S89W0	|	89.0 west Galaxy 28
S91W0	|	95.0 west Nimiq 1
S93W1	|	93.1 west Galaxy 25
S95W0	|	95.0 west Galaxy 3C
S97W0	|	97.0 west Galaxy 19
S99W2	|	99.2 west Galaxy 16 / Spaceway 2
S101W0	|	101.0 west SES 1
S103W0	|	103.0 west AMC 1
S105W0	|	105.0 west AMC 15/18
S107W3	|	107.3 west Anik F1/F1R
S111W1	|	111.1 west Anik F2
S113W0	|	113.0 west SatMex 6
S116W8	|	116.8 west SatMex 5
S119W0	|	119.0 west Anik F3
S121W0	|	121.0 west Echostar 9 / Galaxy 23
S123W0	|	123.0 west Galaxy 18
S125W0	|	125.0 west Galaxy 14 / AMC 21
S127W0	|	127.0 west Galaxy 13 / Horizons 1
S131W0	|	131.0 west AMC 11
S133W0	|	133.0 west Galaxy 15
S135W0	|	135.0 west AMC 10
S137W0	|	135.0 west AMC 7
S139W0	|	135.0 west AMC 8
S177W0	|	177.0 west NSS 9
